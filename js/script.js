$().ready(function(){

    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $(".icons a").hover(function(){

        var offset = $(".icons a").offset();
        var top = parseInt(offset.top);
        top = top - 20;
        if (isNaN(top)) {
            top = 60;
        }

        $(".popup").text( $(this).find("span").text() );
        $(".popup").css({
            'top' : top+'px'
        });
        $(".popup").show("fast");

        delay(function(){ $(".popup").hide("fast"); }, 2000);
    });
    $(".popup").hover(function(){
        delay(function(){ $(".popup").hide("fast"); }, 20000);
    });
    $(".popup").mouseleave(function(){
        delay(function(){ $(".popup").hide("fast"); }, 2000);
    });

});